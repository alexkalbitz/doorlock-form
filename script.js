    //Questions array
    var questions = ["What is the capital of Ohio?", "What color is a zebra?", "How many licks does it take?"];

    var displayQ = questions[Symbol.iterator]();
    
    //Show first question on load
    
    window.onload = function load() {
        var q = document.getElementById("q");
        q.innerHTML = questions[0];
    }
    
    function check(){
        var radios = document.getElementsByClassName("choice");
        var radioValue;
        var correct = 0;
    
        //Display new qusetion after click
        let nextQ = displayQ.next(); // get the next element
    (nextQ.done) ? 
        [q.innerText, this.innerText, this.disabled] = ['Complete!', 'Done', true]
        :q.innerText = nextQ.value;
    
        //Correct answers array
    
        var answers = ["Columbus", "Black & White", "One"];
    
        //Get user radio selection value
    
        for (var i = 0; i < radios.length; i++){
            if (radios[i].checked) {
                radioValue = radios[i].value;
            }
        }
    
        //If no radio value is selected, send alert message
    
        if (radioValue == null){
            alert("Select an answer!");
            return
        } 
    
        //Check answers array against user radio value
    
        for (var j = 0; j <= answers.length; j++){
            if (radioValue === answers[j]){
                correct++;
                document.getElementById("after-submit").style.visibility = "visible";
                document.getElementById("number-correct").innerHTML = "You got " + correct + " correct.";
                return
            } else {
                document.getElementById("after-submit").style.visibility = "visible";
                document.getElementById("number-correct").innerHTML = "Wrong. Try agian.";
                return
            }
        }
    }