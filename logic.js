window.onload = reset() 


function reset(){
    const data = {
        q_list : ["1", "2", "3", "9"],
        cur_idx : 0,
        values:{},
    }
    window.my_data = data
}

const show_cur_q = () => {
    window.my_data.cur
}


// NEXT and BACK BUTTON functionality 
function next() {
    //get current state and elements
    let data = window.my_data
    let idx = data.cur_idx
    if (idx !== 0) {
        let prev_el = document.getElementById(data.q_list[idx - 1])
    }
    // save value of question
    get_value(data.q_list[idx])
    // show/hide next questions
    let cur_el = document.getElementById(data.q_list[idx])
    let next_el = document.getElementById(data.q_list[idx + 1])
    cur_el.hidden = true
    next_el.hidden = false
    data.cur_idx += 1
}



function back() {
    let data = window.my_data
    let idx = data.cur_idx
    // show/hide next questions
    let cur_el = document.getElementById(data.q_list[idx])
    let next_el = document.getElementById(data.q_list[idx - 1])
    cur_el.hidden = true
    next_el.hidden = false
    // change current index
    idx < 1 ? console.log('no prev index') : data.cur_idx -= 1
}

// storing the values of each question
function get_value(id) {
    switch(id){
        case "1":
            get_values_radiobuttons(id)
            break;
        case "2":
            get_values_radiobuttons(id)
            break;
    }

}

function get_values_radiobuttons(id){
    let inputs = document.getElementById(id).getElementsByTagName('input')
    let values = window.my_data.values
    for (let input of inputs) {
        values[input.name] = input.checked   
    }
    console.log(window.my_data.values)
}

function get_values_textinputs(id){
    let el = document.getElementById(id)
    let inputs = el.getElementsByTagName('input')
    let values = window.my_data.values
    for (let input of inputs) {
        values[input.name] = input.value   
    }
    console.log(window.my_data.values)
}


function get_value_dropdown(id){
    let el = document.getElementById(id)
    window.my_data.value.push(el.value)
}


function record_data(el){
    console.log("record data")

}


function radio_val(name){
    const radios = document.getElementsByName(name);
    for (var radio of radios)
    {
        if (radio.checked) {
           console.log(radio.value);
        }
    }
}


function telinput(el){
    el.value = el.value.replace(/[^0-9\+]/g, '').replace(/(\..*?)\..*/g, '$1');
}



function switchLanguage(el){
    // switch all lang class elements hidden status 
    const flags = document.querySelectorAll(".lang")
    flags.forEach( el => {
        el.hidden = !el.hidden
    })
}


function test(){
    console.log("test")
}